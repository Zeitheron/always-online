package com.zeitheron.alwaysonline.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import com.zeitheron.alwaysonline.AlwaysOnlineMod;
import com.zeitheron.hammercore.lib.zlib.error.JSONException;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.json.serapi.IgnoreSerialization;
import com.zeitheron.hammercore.lib.zlib.json.serapi.Jsonable;
import com.zeitheron.hammercore.lib.zlib.utils.MD5;

import net.minecraft.client.Minecraft;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AOHostWrapper
{
	private static Socket $con()
	{
		try
		{
			Socket so = new Socket();
			so.connect(new InetSocketAddress(AlwaysOnlineMod.instance.getServerIP(), AlwaysOnlineMod.instance.getServerPort()), 2000);
			return so;
		} catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		return null;
	}
	
	public static int getHostCount()
	{
		int ret = 0;
		
		try(Socket c = $con())
		{
			if(c == null)
				return 0;
			DataOutputStream dos = new DataOutputStream(c.getOutputStream());
			DataInputStream dis = new DataInputStream(c.getInputStream());
			
			dos.writeByte(0);
			dos.flush();
			
			ret = dis.readInt();
		} catch(IOException ioe)
		{
		}
		
		return ret;
	}
	
	public static HostBody getBody(int index)
	{
		HostBody ret = null;
		
		try(Socket c = $con())
		{
			if(c == null)
				return null;
			DataOutputStream dos = new DataOutputStream(c.getOutputStream());
			DataInputStream dis = new DataInputStream(c.getInputStream());
			
			dos.writeByte(1);
			dos.writeInt(index);
			dos.flush();
			
			byte[] data = new byte[dis.readInt()];
			dis.read(data);
			
			ret = new HostBody();
			Jsonable.deserialize(new String(data), ret);
		} catch(IOException | JSONException ioe)
		{
			ret = null;
		}
		
		return ret;
	}
	
	public static String getIPFromPassword(int index, String password)
	{
		String ret = null;
		
		try(Socket c = $con())
		{
			if(c == null)
				return null;
			DataOutputStream dos = new DataOutputStream(c.getOutputStream());
			DataInputStream dis = new DataInputStream(c.getInputStream());
			byte[] data = (password == null ? "" : password).getBytes();
			dos.writeByte(2);
			dos.writeInt(index);
			dos.writeShort(data.length);
			dos.write(data);
			dos.flush();
			
			if(dis.readBoolean())
			{
				data = new byte[dis.readByte()];
				dis.read(data);
				
				ret = new String(data);
			}
		} catch(IOException ioe)
		{
			ret = null;
		}
		
		return ret;
	}
	
	@SideOnly(Side.CLIENT)
	public static boolean postServer(String password, int port)
	{
		try(Socket c = $con())
		{
			if(c == null)
				return false;
			DataOutputStream dos = new DataOutputStream(c.getOutputStream());
			DataInputStream dis = new DataInputStream(c.getInputStream());
			
			dos.writeByte(10);
			dos.writeInt(port);
			
			byte[] data = Minecraft.getMinecraft().getSession().getProfile().getName().getBytes();
			dos.writeByte(data.length);
			dos.write(data);
			
			data = (password == null || password.isEmpty() ? "" : MD5.encrypt(password)).getBytes();
			dos.writeByte(data.length);
			dos.write(data);
			
			dos.flush();
			
			return dis.readBoolean();
		} catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		
		return false;
	}
	
	public static class HostBody implements Jsonable
	{
		@IgnoreSerialization
		public String passwordMD5;
		
		public String owner;
		
		@Override
		public void deserializeJson(JSONObject json) throws JSONException
		{
			passwordMD5 = json.optString("password");
		}
	}
}