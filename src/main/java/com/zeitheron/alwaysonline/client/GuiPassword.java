package com.zeitheron.alwaysonline.client;

import java.io.IOException;

import com.zeitheron.hammercore.client.utils.RenderUtil;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;

public class GuiPassword extends GuiScreen
{
	final GuiOnline prev;
	final int index;
	GuiTextField pass;
	
	public int invalidTime = 0;
	
	public GuiPassword(GuiOnline prev, int index)
	{
		this.index = index;
		this.prev = prev;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		pass = new GuiTextField(0, fontRenderer, (width - 150) / 2, height / 2 - 22, 150, 20);
		addButton(new GuiButton(0, (width - 150) / 2, height / 2 + 2, 73, 20, I18n.format("gui.back")));
		addButton(new GuiButton(1, (width - 150) / 2 + 78, height / 2 + 2, 73, 20, I18n.format("selectServer.select")));
	}
	
	@Override
	public void updateScreen()
	{
		if(invalidTime > 0)
			--invalidTime;
		super.updateScreen();
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 0)
			mc.displayGuiScreen(prev);
		if(button.id == 1)
		{
			String ip = AOHostWrapper.getIPFromPassword(index, pass.getText());
			if(ip != null)
			{
				String[] data = ip.split(":");
				mc.displayGuiScreen(new GuiConnecting(prev, mc, data[0], Integer.parseInt(data[1])));
			} else
			{
				pass.setText("");
				invalidTime = 100;
			}
		}
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		GlStateManager.disableTexture2D();
		RenderUtil.drawColoredModalRect(0, 0, width, height, 0xFF_36393F);
		GlStateManager.enableTexture2D();
		pass.drawTextBox();
		if(invalidTime > 0)
			drawCenteredString(fontRenderer, I18n.format("password.invalid"), width / 2, height / 2 - 24 - fontRenderer.FONT_HEIGHT, 0xFF2222);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(!pass.textboxKeyTyped(typedChar, keyCode) && keyCode == 1)
			mc.displayGuiScreen(prev);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		if(!pass.mouseClicked(mouseX, mouseY, mouseButton))
			super.mouseClicked(mouseX, mouseY, mouseButton);
	}
}