package com.zeitheron.alwaysonline.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
@EventBusSubscriber(Side.CLIENT)
public class ClientEvents
{
	@SubscribeEvent
	public static void guiInit(GuiScreenEvent.InitGuiEvent e)
	{
		GuiScreen gui = e.getGui();
		
		if(gui instanceof GuiIngameMenu)
		{
			GuiButton lan = null;
			for(GuiButton b : e.getButtonList())
				if(b.id == 7)
				{
					lan = b;
					break;
				}
			if(lan != null)
			{
				lan.width /= 2;
				lan.width -= 1;
				GuiButton share = new GuiButton(0x4635FF, lan.x + lan.width + 3, lan.y, lan.width, lan.height, I18n.format("alwaysOnline.sharews"));
				share.enabled = lan.enabled;
				e.getButtonList().add(share);
			}
		} else if(gui instanceof GuiMainMenu)
		{
			GuiButton mp = null;
			for(GuiButton b : e.getButtonList())
				if(b.id == 2)
				{
					mp = b;
					break;
				}
			if(mp != null)
			{
				e.getButtonList().add(new GuiButton(0x4635FF, mp.x + mp.width + 3, mp.y, mp.width / 2, mp.height, I18n.format("alwaysOnline.online")));
			}
		}
	}
	
	@SubscribeEvent
	public static void actionPerformed(GuiScreenEvent.ActionPerformedEvent e)
	{
		if(e.getGui() instanceof GuiIngameMenu && e.getButton().id == 0x4635FF)
			Minecraft.getMinecraft().displayGuiScreen(new GuiShareWithServer(e.getGui()));
		else if(e.getGui() instanceof GuiMainMenu && e.getButton().id == 0x4635FF)
			Minecraft.getMinecraft().displayGuiScreen(new GuiOnline());
	}
}