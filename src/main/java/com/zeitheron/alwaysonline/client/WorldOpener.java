package com.zeitheron.alwaysonline.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.util.function.Consumer;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.zeitheron.alwaysonline.AlwaysOnlineMod;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.lib.zlib.utils.MD5;
import com.zeitheron.hammercore.lib.zlib.weupnp.AttuneResult;
import com.zeitheron.hammercore.lib.zlib.weupnp.EnumProtocol;
import com.zeitheron.hammercore.lib.zlib.weupnp.WeUPnP;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.GameType;

public class WorldOpener
{
	public static final Consumer<String> CHAT = msg ->
	{
		EntityPlayer receiver = Minecraft.getMinecraft().player;
		if(receiver != null)
			receiver.sendMessage(new TextComponentString(msg));
	};
	
	public static String getExtIP()
	{
		try(BufferedReader br = new BufferedReader(new InputStreamReader(new URL("http://checkip.amazonaws.com/").openConnection().getInputStream())))
		{
			return br.readLine();
		} catch(IOException ioe)
		{
		}
		return null;
	}
	
	public static String shareWorld(String gameMode, String password, boolean allowCheats)
	{
		CHAT.accept("Sharing with world...");
		
		String pstr = Minecraft.getMinecraft().getIntegratedServer().shareToLAN(GameType.getByName(gameMode), allowCheats);
		if(pstr != null)
		{
			int port = Integer.parseInt(pstr);
			String ip = getExtIP();
			
			if(ip == null)
			{
				CHAT.accept("Unable to obtain external ip (connection to \"http://checkip.amazonaws.com/\" failed). Aborting share.");
				return null;
			}
			
			try
			{
				Socket sock = new Socket();
				sock.setSoTimeout(5000);
				sock.connect(new InetSocketAddress(ip, port));
				sock.close();
				
				CHAT.accept("Connection valid.");
			} catch(IOException se)
			{
				CHAT.accept("Normal connection failed. Attempting port-forward!");
				
				AttuneResult res = null;
				
				try
				{
					WeUPnP p = new WeUPnP();
					p.setup();
					p.discover();
					p.logFound(AlwaysOnlineMod.LOG);
					res = p.attune(EnumProtocol.TCP, port, port, Minecraft.getMinecraft().getIntegratedServer().getMOTD());
					CHAT.accept("Port-forward complete. Validating connection...");
					
					Socket sock = new Socket();
					sock.setSoTimeout(5000);
					sock.connect(new InetSocketAddress(ip, port));
					sock.close();
					
					CHAT.accept("Connection valid.");
				} catch(IOException | SAXException | ParserConfigurationException ioe)
				{
					CHAT.accept("Port-forward impossible. Aborting share.");
					try
					{
						res.undo();
					} catch(IOException | SAXException e)
					{
						e.printStackTrace();
					}
					return null;
				}
				
				HammerCore.closeAfterLogoff.add(res);
			}
			
			// Share to server
			if(AOHostWrapper.postServer(password, port))
				CHAT.accept("Share complete!");
			else
				CHAT.accept("Share failed! :(");
		} else
			CHAT.accept("Unable to open LAN world. Aborting share.");
		
		return null;
	}
}