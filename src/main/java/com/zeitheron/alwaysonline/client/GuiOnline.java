package com.zeitheron.alwaysonline.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Predicate;

import org.lwjgl.input.Mouse;

import com.zeitheron.alwaysonline.AlwaysOnlineMod;
import com.zeitheron.alwaysonline.client.AOHostWrapper.HostBody;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.init.SoundEvents;
import net.minecraft.tileentity.TileEntitySkull;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextFormatting;

public class GuiOnline extends GuiScreen
{
	protected static ExecutorService mpexec;
	public static List<HostBody> hosts = new ArrayList<>();
	{
		if(mpexec != null)
			mpexec.shutdownNow();
		hosts.clear();
		mpexec = Executors.newFixedThreadPool(1);
		mpexec.execute(() ->
		{
			int amt = AOHostWrapper.getHostCount();
			AlwaysOnlineMod.LOG.debug("Get hosts... " + amt);
			for(int i = 0; i < amt; ++i)
			{
				HostBody body = AOHostWrapper.getBody(i);
				if(body != null)
					hosts.add(body);
			}
		});
	}
	
	public int scroll;
	public int prevScroll;
	public HostBody bodyOverMouse;
	
	public GuiTextField search;
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		GuiTextField newSearch = new GuiTextField(0, fontRenderer, 4, 3, 200, 17);
		newSearch.setMaxStringLength(256);
		if(search != null)
		{
			newSearch.setText(search.getText());
			newSearch.setCursorPosition(search.getCursorPosition());
			newSearch.setSelectionPos(search.getSelectionEnd());
		}
		search = newSearch;
		
		addButton(new GuiButton(0, width - 152, 2, 150, 20, I18n.format("gui.back")));
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 0)
			mc.displayGuiScreen(null);
	}
	
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		prevScroll = scroll;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		GlStateManager.disableTexture2D();
		RenderUtil.drawColoredModalRect(0, 0, width, height, 0xFF_36393F);
		GlStateManager.enableTexture2D();
		
		int dw = -Mouse.getDWheel();
		scroll += dw / 10;
		
		GlStateManager.enableBlend();
		
		scroll = Math.max(0, scroll);
		prevScroll = Math.max(0, prevScroll);
		
		int items = hosts.size();
		
		int maxScroll = (items * 34) - (height / 34 - 1) * 34 + 40;
		scroll = Math.min(maxScroll, scroll);
		prevScroll = Math.min(maxScroll, prevScroll);
		
		float ascroll = prevScroll + (scroll - prevScroll) * partialTicks;
		
		float y = 28 - ascroll % 34;
		int startX = 4;
		
		for(int i = Math.max(0, ((int) (ascroll / 34))); i < items; ++i)
		{
			HostBody body = hosts.get(i);
			
			boolean hover = mouseX >= startX && mouseY >= Math.max(24, y) && mouseX < width - 34 && mouseY < Math.min(height - 18, y + 34);
			
			if(hover)
				bodyOverMouse = body;
			
			GlStateManager.pushMatrix();
			GlStateManager.translate(startX + 6, y, 0);
			{
				if(hover)
				{
					GlStateManager.disableTexture2D();
					RenderUtil.drawColoredModalRect(-startX, 0, width, 34, 0xFF_27292D);
					GlStateManager.enableTexture2D();
				}
				
				UtilsFX.bindTexture(SkinHelper.getSkin(SkinHelper.getProfile(body.owner)));
				RenderUtil.drawTexturedModalRect(0, 1, 32, 32, 32, 32);
				RenderUtil.drawTexturedModalRect(0, 1, 128 + 32, 32, 32, 32);
				fontRenderer.drawString(body.owner + TextFormatting.DARK_GRAY + " #" + i, 34, 2, 0xFFFFFF);
			}
			GlStateManager.popMatrix();
			
			y += 34;
			if(y > height)
				break;
			
			GlStateManager.color(1F, 1F, 1F, 1F);
		}
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(0, 0, 500);
		GlStateManager.disableTexture2D();
		RenderUtil.drawColoredModalRect(0, 0, width, 24, 0xFF_2F3136);
		RenderUtil.drawColoredModalRect(0, height - 18, width, 18, 0xFF_2F3136);
		
		float h = Math.max((height - 42) / (float) maxScroll * 64F, 24);
		RenderUtil.drawColoredModalRect(width - 12, 24, 12, height - 42, 0xFF_33363A);
		RenderUtil.drawColoredModalRect(width - 12, 24 + (height - h - 42) * (ascroll / (float) maxScroll), 12, h, 0xFF_202225);
		
		// if(search != null)
		// search.drawTextBox();
		
		GlStateManager.enableBlend();
		
		GlStateManager.enableTexture2D();
		GlStateManager.popMatrix();
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		// if(!search.textboxKeyTyped(typedChar, keyCode))
		super.keyTyped(typedChar, keyCode);
	}
	
	int anchorX, anchorY;
	
	public long lc = 0;
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		// if(!search.mouseClicked(mouseX, mouseY, mouseButton))
		{
			super.mouseClicked(mouseX, mouseY, mouseButton);
			
			if(mouseButton == 1 && search.isFocused())
			{
				search.setText("");
			}
			
			if(bodyOverMouse != null && mouseButton == 0)
			{
				if(System.currentTimeMillis() - lc < 400L)
				{
					mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1F));
					
					// Connect!
					if(bodyOverMouse.passwordMD5 == null || bodyOverMouse.passwordMD5.isEmpty())
					{
						String ip = AOHostWrapper.getIPFromPassword(hosts.indexOf(bodyOverMouse), null);
						if(ip != null)
						{
							String[] data = ip.split(":");
							mc.displayGuiScreen(new GuiConnecting(this, mc, data[0], Integer.parseInt(data[1])));
						}
					} else
						mc.displayGuiScreen(new GuiPassword(this, hosts.indexOf(bodyOverMouse)));
				} else
					lc = System.currentTimeMillis();
			}
			
			if(mouseButton == 0)
			{
				anchorX = mouseX;
				anchorY = mouseY;
				
				int items = hosts.size();
				int maxScroll = (items * 34) - (height / 34 - 1) * 34 + 40;
				float h = Math.max((height - 42) / (float) maxScroll * 64F, 24);
				
				float xh = height - h - 42;
				
				if(anchorX >= width - 12 && anchorY >= 24 && anchorX < width && anchorY < height - 18)
				{
					float p = Math.max(0, Math.min(1, (mouseY - 24 - h / 2) / xh));
					scroll = (int) (p * maxScroll);
				}
			}
		}
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		int items = hosts.size();
		int maxScroll = (items * 34) - (height / 34 - 1) * 34 + 40;
		float h = Math.max((height - 42) / (float) maxScroll * 64F, 24);
		
		float xh = height - h - 42;
		
		if(anchorX >= width - 12 && anchorY >= 24 && anchorX < width && anchorY < height - 18)
		{
			float p = Math.max(0, Math.min(1, (mouseY - 24 - h / 2) / xh));
			scroll = (int) (p * maxScroll);
		}
	}
}