package com.zeitheron.alwaysonline.client;

import java.io.IOException;

import com.zeitheron.hammercore.lib.zlib.utils.Threading;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiShareToLan;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.GameType;

public class GuiShareWithServer extends GuiScreen
{
	private final GuiScreen lastScreen;
	private GuiButton allowCheatsButton;
	private GuiButton gameModeButton;
	private String gameMode = "survival";
	private boolean allowCheats;
	public GuiTextField password;
	
	public GuiShareWithServer(GuiScreen lastScreenIn)
	{
		this.lastScreen = lastScreenIn;
	}
	
	@Override
	public void initGui()
	{
		this.buttonList.clear();
		this.buttonList.add(new GuiButton(101, this.width / 2 - 155, this.height - 28, 150, 20, I18n.format("lanServer.start")));
		this.buttonList.add(new GuiButton(102, this.width / 2 + 5, this.height - 28, 150, 20, I18n.format("gui.cancel")));
		this.gameModeButton = this.addButton(new GuiButton(104, this.width / 2 - 155, 100, 150, 20, I18n.format("selectWorld.gameMode")));
		this.allowCheatsButton = this.addButton(new GuiButton(103, this.width / 2 + 5, 100, 150, 20, I18n.format("selectWorld.allowCommands")));
		this.updateDisplayNames();
		
		buttonList.get(0).displayString = I18n.format("alwaysOnline.share");
		
		String text = password != null ? password.getText() : "";
		password = new GuiTextField(0, fontRenderer, this.width / 2 - 155, 140, 304, 20);
		password.setText(text);
	}
	
	private void updateDisplayNames()
	{
		this.gameModeButton.displayString = I18n.format("selectWorld.gameMode") + ": " + I18n.format("selectWorld.gameMode." + this.gameMode);
		this.allowCheatsButton.displayString = I18n.format("selectWorld.allowCommands") + " ";
		
		if(this.allowCheats)
		{
			this.allowCheatsButton.displayString = this.allowCheatsButton.displayString + I18n.format("options.on");
		} else
		{
			this.allowCheatsButton.displayString = this.allowCheatsButton.displayString + I18n.format("options.off");
		}
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
        drawCenteredString(fontRenderer, I18n.format("lanServer.title"), this.width / 2, 50, 16777215);
        drawCenteredString(fontRenderer, I18n.format("lanServer.otherPlayers"), this.width / 2, 82, 16777215);
		drawCenteredString(fontRenderer, I18n.format("alwaysOnline.password") + ":", this.width / 2, 126, 16777215);
		password.drawTextBox();
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return true;
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(!password.textboxKeyTyped(typedChar, keyCode))
			super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 102)
		{
			this.mc.displayGuiScreen(this.lastScreen);
		} else if(button.id == 104)
		{
			if("spectator".equals(this.gameMode))
				this.gameMode = "creative";
			else if("creative".equals(this.gameMode))
				this.gameMode = "adventure";
			else if("adventure".equals(this.gameMode))
				this.gameMode = "survival";
			else
				this.gameMode = "spectator";
			this.updateDisplayNames();
		} else if(button.id == 103)
		{
			this.allowCheats = !this.allowCheats;
			this.updateDisplayNames();
		} else if(button.id == 101)
		{
			mc.displayGuiScreen(null);
			Threading.createAndStart("AlwaysOnlineShare", () -> WorldOpener.shareWorld(gameMode, password.getText(), allowCheats));
		}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		if(!password.mouseClicked(mouseX, mouseY, mouseButton))
			super.mouseClicked(mouseX, mouseY, mouseButton);
	}
}