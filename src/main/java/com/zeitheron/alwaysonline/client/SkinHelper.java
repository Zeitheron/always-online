package com.zeitheron.alwaysonline.client;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.minecraft.MinecraftProfileTexture.Type;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntitySkull;
import net.minecraft.util.ResourceLocation;

public class SkinHelper
{
	private static final Map<String, GameProfile> profiles = new HashMap<>();
	
	public static GameProfile getProfile(String username)
	{
		if(!profiles.containsKey(username))
		{
			GameProfile gp = TileEntitySkull.updateGameProfile(new GameProfile(null, username));
			profiles.put(username, gp);
			return gp;
		}
		return profiles.get(username);
	}
	
	public static ResourceLocation getSkin(GameProfile profile)
	{
		ResourceLocation resourcelocation = DefaultPlayerSkin.getDefaultSkinLegacy();
		
		if(profile != null)
		{
			Minecraft minecraft = Minecraft.getMinecraft();
			Map<Type, MinecraftProfileTexture> map = minecraft.getSkinManager().loadSkinFromCache(profile);
			
			if(map.containsKey(Type.SKIN))
			{
				resourcelocation = minecraft.getSkinManager().loadSkin(map.get(Type.SKIN), Type.SKIN);
			} else
			{
				UUID uuid = EntityPlayer.getUUID(profile);
				resourcelocation = DefaultPlayerSkin.getDefaultSkin(uuid);
			}
		}
		
		return resourcelocation;
	}
}