package com.zeitheron.alwaysonline;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.alwaysonline.host.AlwaysOnlineHost;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.network.NetworkCheckHandler;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = "alwaysonline", name = "Always Online", version = "@VERSION@", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97", dependencies = "required-after:hammercore")
public class AlwaysOnlineMod
{
	@NetworkCheckHandler
	public static boolean checkNet(Map<String, String> mods, Side side)
	{
		return true;
	}
	
	@Instance
	public static AlwaysOnlineMod instance;
	
	public static Configuration config;
	
	public static final Logger LOG = LogManager.getLogger("AlwaysOnline");
	
	public static final AlwaysOnlineHost HOST = new AlwaysOnlineHost();
	
	public static Side runtimeSide;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		config = new Configuration(e.getSuggestedConfigurationFile(), "@VERSION@");
		runtimeSide = e.getSide();
		
		if(runtimeSide == Side.CLIENT)
		{
			getServerIP();
			getServerPort();
		} else
			HOST.start(getHostPort());
		
		if(config.hasChanged())
			config.save();
	}
	
	@EventHandler
	public void serverStop(FMLServerStoppingEvent e)
	{
		if(runtimeSide == Side.SERVER)
			HOST.stop();
	}
	
	public String getServerIP()
	{
		return config.getString("server ip", "connection", "alwaysonlinemod.tk", "What IP should AlwaysOnline use as server source?");
	}
	
	public int getServerPort()
	{
		return config.getInt("server port", "connection", 25593, 0, 65535, "What port should AlwaysOnline use for server source?");
	}
	
	public int getHostPort()
	{
		return config.getInt("host port", "connection", 25593, 0, 65535, "What port should AlwaysOnline use for to HOST?");
	}
}